//
//  ContactDto.swift
//  sample-application
//
//  Created by Alessandro on 11/10/21.
//

import Foundation

public struct ContactDto: Equatable {

    public var name: String
    public var surname: String
    public var avatar: String { get {
        return "https://i.pravatar.cc/500/" + name + "/" + surname
    }}

    public init(name: String, surname: String) {
        self.name = name
        self.surname = surname
    }
    
    public static func == (lhs: ContactDto, rhs: ContactDto) -> Bool {
        return
            lhs.name == rhs.name &&
            lhs.surname == rhs.surname &&
            lhs.avatar == rhs.avatar
    }
}
