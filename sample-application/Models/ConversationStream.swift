//
//  ConversationStream.swift
//  sample-application
//
//  Created by Alessandro on 13/10/21.
//

import Foundation
import RxSwift
import RxCocoa

public struct ConversationStream: Equatable {

    public var contact: ContactDto
    public var hasAudio: BehaviorRelay<Bool>
    public var hasVideo: BehaviorRelay<Bool>

    public init(contact: ContactDto) {
        self.contact = contact
        
        hasAudio = BehaviorRelay<Bool>(value: true)
        hasVideo = BehaviorRelay<Bool>(value: true)
    }
    
    public static func == (lhs: ConversationStream, rhs: ConversationStream) -> Bool {
        return lhs.contact == rhs.contact
    }
}
