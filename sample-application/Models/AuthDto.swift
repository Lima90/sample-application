//
//  AuthDto.swift
//  sample-application
//
//  Created by Alessandro on 10/10/21.
//

import Foundation

public struct AuthDto: Codable {

    public var userId: String?
    public var username: String?

    public init(userId: String?, username: String?) {
        self.userId = userId
        self.username = username
    }
    
    public init(JSONString: String) {
        
        // Decode
        let jsonData = JSONString.data(using: .utf8)!
        let decoder = JSONDecoder()
        let auth = try! decoder.decode(AuthDto.self, from: jsonData)
        
        self = auth
    }
    
    public func toJSONString(prettyPrint: Bool) -> String {
        
        // Encoder
        let encoder = JSONEncoder()
        if prettyPrint {
            encoder.outputFormatting = .prettyPrinted
        }
        let data = try! encoder.encode(self)
        return String(data: data, encoding: .utf8)!
    }
}
