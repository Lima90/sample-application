//
//  CredentialsDto.swift
//  sample-application
//
//  Created by Alessandro on 11/10/21.
//

import Foundation

public struct CredentialsDto: Codable {

    public var username: String
    public var password: String?

    public init(username: String, password: String?) {
        self.username = username
        self.password = password
    }
}
