//
//  ConversationViewController.swift
//  sample-application
//
//  Created by Alessandro on 13/10/21.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa
import RxDataSources
import Swinject
import RxOrientation
import AVFoundation

class ConversationViewController: BaseViewController {

    // MARK: - Properties
    // MARK: Class
    
    
    // MARK: Public
    
    let viewModel: ConversationViewModelProtocol
    
    
    // MARK: Private
    private var buttonsVisible = false
    
    // UIFields
    var tap: UITapGestureRecognizer?
    var streamsParentView = UIView()
    let stramsStackView = UIStackView()
    var innerStk1 = UIStackView()
    var innerStk2 = UIStackView()
    var buttonsParentView = UIView()
    var myCameraView = UIView()
    var hangUpBtn = UIButton()
    var muteUnmuteBtn = UIButton()
    var showHideCameraBtn = UIButton()
    var stackView = UIStackView()
    private var buttonsParentViewBottomCnst: Constraint?
    private var conversationContactViewList: [ConversationContactView] = []
    
    //My video preview obj
    var captureSession: AVCaptureSession!
    var videoPreviewLayer: AVCaptureVideoPreviewLayer!
    
    // Output
    
    // MARK: - Methods
    // MARK: Class
    
    
    // MARK: Lifecycle
    required init?(coder aDecoder: NSCoder) {
        fatalError("ContactListViewController must be initialized from init(viewModel:)")
    }
    
    init(viewModel: ConversationViewModelProtocol) {
        self.viewModel = viewModel
        
        super.init()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        buttonsParentView.roundCorners(corners: [.topRight, .topLeft], radius: 36)
        
        if (self.videoPreviewLayer != nil) {
            self.videoPreviewLayer.frame = self.myCameraView.bounds
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tap = UITapGestureRecognizer(target: self, action: #selector(showHideButtonsView))
        streamsParentView.addGestureRecognizer(tap!)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.viewModel.roomService?.connect()
        
        initializeCameraPreview()
    }
    
    private func initializeCameraPreview() {
        captureSession = AVCaptureSession()
        captureSession.sessionPreset = .medium
        
        guard let frontCamera = AVCaptureDevice.default(.builtInWideAngleCamera, for: AVMediaType.video, position: .front)
            else {
                print("Impossibile accedere alla fotocamera frontale")
                return
        }
        
        do {
            let input = try AVCaptureDeviceInput(device: frontCamera)
            
            if captureSession.canAddInput(input) {
                captureSession.addInput(input)
                setupLivePreview()
                
                DispatchQueue.global(qos: .userInitiated).async {
                    self.captureSession.startRunning()
                }
            }
        }
        catch let error  {
            print("errore nell'inizializzare la fotocamera frontale:  \(error.localizedDescription)")
        }
    }
    
    func setupLivePreview() {
        
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        
        if let videoOrientation = UIDevice.current.orientation.videoOrientation {
            videoPreviewLayer.connection?.videoOrientation = videoOrientation
        }
        
        videoPreviewLayer.videoGravity = .resizeAspectFill
        myCameraView.layer.addSublayer(videoPreviewLayer)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.captureSession.stopRunning()
    }
    
    // MARK: Custom accessors
    
    
    // MARK: IBActions
    
    
    
    // MARK: Private
    override func setupUi() {
        self.view.backgroundColor = .black
        
        streamsParentView.backgroundColor = .clear
        streamsParentView.isHidden = true
        streamsParentView.alpha = 0
        view.addSubview(streamsParentView)
        
        stramsStackView.distribution = .fillEqually
        stramsStackView.spacing = 0
        stramsStackView.alignment = .fill
        stramsStackView.backgroundColor = .clear
        
        
        innerStk1.distribution = .fillEqually
        innerStk1.spacing = 0
        innerStk1.alignment = .fill
        innerStk1.backgroundColor = .clear
        innerStk1.axis = .horizontal
        
        
        innerStk2.distribution = .fillEqually
        innerStk2.spacing = 0
        innerStk2.alignment = .fill
        innerStk2.backgroundColor = .clear
        innerStk2.axis = .horizontal
        
        conversationContactViewList = []
        if (self.viewModel.roomService!.contactList.count > 4 || self.viewModel.roomService!.contactList.count == 0) {
            
            fatalError("unsupported contactList size")
            
        } else if (self.viewModel.roomService!.contactList.count > 2) {
            
            stramsStackView.addArrangedSubview(innerStk1)
            
            for index in 0...1 {
                let cView = ConversationContactView()
                cView.contact = self.viewModel.roomService!.contactList[index]
                innerStk1.addArrangedSubview(cView)
                conversationContactViewList.append(cView)
            }
            
            if (self.viewModel.roomService!.contactList.count == 4) {
                stramsStackView.addArrangedSubview(innerStk2)
                
                for index in 2...3 {
                    let cView = ConversationContactView()
                    cView.contact = self.viewModel.roomService!.contactList[index]
                    innerStk2.addArrangedSubview(cView)
                    conversationContactViewList.append(cView)
                }
            } else {
                
                for index in 2...2 {
                    let cView = ConversationContactView()
                    cView.contact = self.viewModel.roomService!.contactList[index]
                    stramsStackView.addArrangedSubview(cView)
                    conversationContactViewList.append(cView)
                }
            }
            
        } else {
            
            for c in self.viewModel.roomService!.contactList {
                let cView = ConversationContactView()
                cView.contact = c
                stramsStackView.addArrangedSubview(cView)
                conversationContactViewList.append(cView)
            }
        }
        
        orientationChanged(orientaion: UIDevice.current.orientation)
        
        streamsParentView.addSubview(stramsStackView)
        
        myCameraView.backgroundColor = UIColor(hex: "#333333")
        myCameraView.layer.cornerRadius = 20
        myCameraView.clipsToBounds = true
        view.addSubview(myCameraView)
        
        buttonsParentView.backgroundColor = .darkGray
        
        view.addSubview(buttonsParentView)
        
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.spacing = 18
        stackView.alignment = .top
        
        muteUnmuteBtn.backgroundColor = .gray
        muteUnmuteBtn.layer.cornerRadius = 25
        muteUnmuteBtn.contentEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        muteUnmuteBtn.setImage(UIImage(named: "mic"), for: .normal)
        muteUnmuteBtn.imageView?.contentMode = .scaleAspectFit
        
        showHideCameraBtn.backgroundColor = .gray
        showHideCameraBtn.layer.cornerRadius = 25
        showHideCameraBtn.contentEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        showHideCameraBtn.setImage(UIImage(named: "cam"), for: .normal)
        showHideCameraBtn.imageView?.contentMode = .scaleAspectFit
        
        hangUpBtn.backgroundColor = .red
        hangUpBtn.layer.cornerRadius = 25
        hangUpBtn.contentEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        hangUpBtn.setImage(UIImage(named: "hangUp"), for: .normal)
        hangUpBtn.imageView?.contentMode = .scaleAspectFit
        
        stackView.addArrangedSubview(muteUnmuteBtn)
        stackView.addArrangedSubview(showHideCameraBtn)
        stackView.addArrangedSubview(hangUpBtn)
        
        buttonsParentView.addSubview(stackView)
    }
    
    
    override func setupBindings() {
        super.setupBindings()
        
        viewModel.isLoading
            .asDriver()
            .drive(hudIsVisible)
            .disposed(by: disposeBag)
        
        self.viewModel.roomService?.connectionState
            .observeOn(MainScheduler.instance)
            .bind(onNext: { [weak self] state in
            guard let self = self else { return }
            
            switch state {
                case .connecting:
                    self.viewModel.isLoading.accept(true)
                    break
                case .connected:
                    self.streamsParentView.isHidden = false
                    self.streamsParentView.fadeIn()
                    self.viewModel.isLoading.accept(false)
                    break
                default:
                    self.viewModel.isLoading.accept(false)
                    break
            }
        }).disposed(by: disposeBag)
        
        self.viewModel.roomService?.connectedStreams
            .observeOn(MainScheduler.instance)
            .bind(onNext: { [weak self] connectedStreams in
                guard let self = self else { return }
                
                for stream in connectedStreams {
                    
                    if let convView = self.conversationContactViewList.first(where: { $0.contact == stream.contact }) {
                        convView.stream = stream
                    }
                        
                }
            }).disposed(by: disposeBag)
        
        muteUnmuteBtn.rx.tap.throttle(.milliseconds(200), scheduler: MainScheduler.instance)
            .bind(to: viewModel.micTap)
            .disposed(by: disposeBag)
        
        showHideCameraBtn.rx.tap.throttle(.milliseconds(200), scheduler: MainScheduler.instance)
            .bind(to: viewModel.camTap)
            .disposed(by: disposeBag)
        
        hangUpBtn.rx.tap.throttle(.seconds(1), scheduler: MainScheduler.instance)
            .bind(to: viewModel.hangUpTap)
            .disposed(by: disposeBag)
        
        self.viewModel.roomService?.camOn
            .asObservable()
            .bind(onNext: { [weak self] on in
                guard let self = self else { return }
                self.showHideCameraBtn.setImage(UIImage(named: on ? "cam" : "camOff"), for: .normal)
                self.myCameraView.isHidden = !on
            }).disposed(by: disposeBag)
        
        self.viewModel.roomService?.micOn
            .asObservable()
            .bind(onNext: { [weak self] on in
                guard let self = self else { return }
                self.muteUnmuteBtn.setImage(UIImage(named: on ? "mic" : "micOff"), for: .normal)
            }).disposed(by: disposeBag)
        
        UIDevice.current.rx.orientation.asObservable()
            .subscribe(onNext: { [weak self] orientaion in
                guard let self = self else { return }
                
                self.orientationChanged(orientaion: orientaion)
                
            }).disposed(by: disposeBag)
    }
    
    private func orientationChanged(orientaion: UIDeviceOrientation) {
        self.stramsStackView.axis = orientaion.isLandscape ? .horizontal : .vertical
        self.innerStk1.axis = orientaion.isLandscape ? .vertical : .horizontal
        self.innerStk2.axis = orientaion.isLandscape ? .vertical : .horizontal
        
        if let videoPrevLayer = self.videoPreviewLayer, let previewLayerConnection = videoPrevLayer.connection, let videoOrientation = orientaion.videoOrientation, previewLayerConnection.isVideoOrientationSupported {
            previewLayerConnection.videoOrientation = videoOrientation
        }
    }
    
    override func setupConstraints() {
        streamsParentView.snp.makeConstraints { (make) in
            make.left.right.top.bottom.equalToSuperview()
        }
        
        stramsStackView.snp.makeConstraints { (make) in
            make.left.right.top.bottom.equalToSuperview()
        }
        
        buttonsParentView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.height.equalTo(100)
            self.buttonsParentViewBottomCnst = make.top.equalTo(self.view.snp.bottom).constraint
        }
        
        myCameraView.snp.makeConstraints { (make) in
            make.right.equalToSuperview().inset(18)
            make.height.width.equalTo(130)
            make.bottom.equalTo(buttonsParentView.snp.top).offset(-18)
        }
        
        stackView.snp.makeConstraints { (make) in
            make.left.top.right.bottom.equalToSuperview().inset(18)
        }
        
        muteUnmuteBtn.snp.makeConstraints { (make) in
            make.height.height.equalTo(50)
        }
        
        showHideCameraBtn.snp.makeConstraints { (make) in
            make.height.height.equalTo(50)
        }
        
        hangUpBtn.snp.makeConstraints { (make) in
            make.height.height.equalTo(50)
        }
    }
    
    @objc private func showHideButtonsView(){
        
        setSaveButtonsVisibility(isVisible: !buttonsVisible, animated: true)
    }
    
    
    func setSaveButtonsVisibility(isVisible: Bool, animated: Bool) {
        buttonsVisible = isVisible
        let updateConstraintCallback = {
            let offset = isVisible ? -self.buttonsParentView.bounds.height : 0
            self.buttonsParentViewBottomCnst?.update(offset: offset)
            self.view.layoutIfNeeded()
        }
        if animated {
            UIView.animate(withDuration: 0.2, animations: {
                var buttonsFrame = self.buttonsParentView.frame
                var myCameraFrame = self.myCameraView.frame
                if isVisible {
                    buttonsFrame.origin.y -= buttonsFrame.size.height
                    myCameraFrame.origin.y -= buttonsFrame.size.height
                } else {
                    buttonsFrame.origin.y += buttonsFrame.size.height
                    myCameraFrame.origin.y += buttonsFrame.size.height
                }
                self.buttonsParentView.frame = buttonsFrame
                self.myCameraView.frame = myCameraFrame
            }, completion: { finished in
                updateConstraintCallback()
            })
        } else {
            updateConstraintCallback()
        }
    }
}

