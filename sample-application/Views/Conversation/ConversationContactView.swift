//
//  ConversationContactView.swift
//  sample-application
//
//  Created by Alessandro on 14/10/21.
//

import Foundation
import UIKit
import RxSwift

final class ConversationContactView: UIView {
    
    // MARK: Private
    private let disposeBag = DisposeBag()
    private var _contact: ContactDto?
    private var _stream: ConversationStream?
    
    // UIFields
    var connectingLabel = UILabel()
    var streamImgView = UIImageView()
    
    // MARK: Public
    public var contact: ContactDto? {
        get {
            return _contact
        }
        set {
            _contact = newValue
            updateUi()
        }
    }
    
    public var stream: ConversationStream? {
        get {
            return _stream
        }
        set {
            _stream = newValue
            updateUi()
        }
    }
    
    // MARK: Lifecycle
    init() {
        
        super.init(frame: .zero)
        setupUi()
        setupConstraints()
        setupBindings()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Private
    private func setupUi() {
        backgroundColor = .clear
        clipsToBounds = true
        
        connectingLabel.textColor = .white
        connectingLabel.textAlignment = .center
        connectingLabel.numberOfLines = 5
        
        addSubview(connectingLabel)
        
        streamImgView.backgroundColor = .clear
        streamImgView.contentMode = .scaleAspectFill
        
        addSubview(streamImgView)
    }
    
    private func setupConstraints() {
        connectingLabel.snp.makeConstraints { (make) in
            make.left.right.top.bottom.equalToSuperview().inset(18)
        }
        
        streamImgView.snp.makeConstraints { (make) in
            make.left.right.top.bottom.equalToSuperview()
        }
    }
    
    private func setupBindings() {
        
    }
    
    private func updateUi() {
        if (stream != nil) {
            streamImgView.isHidden = false
            streamImgView.load(urlString: stream!.contact.avatar)
        } else {
            streamImgView.isHidden = true
        }
        
        if (contact != nil) {
            connectingLabel.text = "Connessione in corso: \n " + contact!.name + " " + contact!.surname
        }
    }
}
