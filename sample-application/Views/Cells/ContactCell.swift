//
//  ContactCell.swift
//  sample-application
//
//  Created by Alessandro on 12/10/21.
//

import UIKit
import RxSwift

/**
* Cella di una tabella semplice: con titolo e simbolo di disclosure
*/
class ContactCell: UITableViewCell {
    // MARK: - Properties
    // MARK: Class
    static let reuseId = "ContactCell"
    
    // MARK: Public
    var disposeBag = DisposeBag()
    var contact: ContactDto?
    
    // MARK: Private
    
    // MARK: - Methods
    // MARK: Class
    
    
    // MARK: Lifecycle
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupUI()
        setupConstraints()
        setupBindings()
        
    }
    
    override func prepareForReuse() {
       super.prepareForReuse()
       disposeBag = DisposeBag() // because life cicle of every cell ends on prepare for reuse
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
//        //set the values for top,left,bottom,right margins
//        let margins = UIEdgeInsets(top: 2, left: 0, bottom: 2, right: 0)
//        contentView.frame = contentView.frame.inset(by: margins)
    }
    
    // MARK: Public
    public func configure(with contact: ContactDto) {
        self.contact = contact
        
        var nameAttributedString = NSMutableAttributedString(string:contact.name + " ")
        
        var attrs = [NSAttributedString.Key.font : UIFont.bold15]
        var boldString = NSMutableAttributedString(string: contact.surname, attributes:attrs)

        nameAttributedString.append(boldString)
        
        textLabel?.attributedText = nameAttributedString
        let img = UIImageView.init(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        img.backgroundColor = .lightGray
        img.layer.cornerRadius = 25
        img.load(urlString: contact.avatar)
        img.clipsToBounds = true
        accessoryView = img
    }
    
    
    
    // MARK: Private
    private func setupUI() {
        
        // UI
        backgroundColor = .clear
        
        let bgView = UIView()
        bgView.backgroundColor = .clear
        backgroundView = bgView
        
        let selBgView = UIView()
        selBgView.backgroundColor = .primaryLight
        selectedBackgroundView = selBgView
        
        // Titolo
        textLabel?.font = UIFont.regular15
        textLabel?.textColor = .black
        textLabel?.textAlignment = .natural
        textLabel?.numberOfLines = 1
        textLabel?.lineBreakMode = .byTruncatingTail
    }
    
    private func setupConstraints() {
        
        // Aggiunta delle views
        
        // Constraints
        backgroundView?.snp.makeConstraints({ (make) in
            make.left.right.equalToSuperview()
            make.top.bottom.equalTo(contentView)
        })
        
    }
    
    private func setupBindings() {
        
    }

}

