//
//  ExtendedTextField.swift
//  sample-application
//
//  Created by Alessandro on 11/10/21.
//

import Foundation
import UIKit

extension UITextField {

    func setBottomBorder() {
        let line = UIView()
        line.frame.size = CGSize(width: self.frame.size.width, height: 1)
        line.frame.origin = CGPoint(x: 0, y: self.frame.maxY - line.frame.height)
        line.backgroundColor = UIColor.primary
        line.autoresizingMask = [.flexibleWidth, .flexibleTopMargin]
        self.addSubview(line)
        self.tintColor = .primary
    }
}
