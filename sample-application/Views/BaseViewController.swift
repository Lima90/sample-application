//
//  BaseViewController.swift
//  sample-application
//
//  Created by Alessandro on 10/10/21.
//

import UIKit
import RxSwift
import RxCocoa
import JGProgressHUD

class BaseViewController: UIViewController {
    
    // MARK: - Properties
    // MARK: Class
    
    
    // MARK: Public
    let hudIsVisible = BehaviorRelay<Bool>(value: false)
    
    var nav: BaseNavigationController? {
        return navigationController as? BaseNavigationController
    }
    
    internal var _backBtn: UIBarButtonItem?
    var backBtn: UIBarButtonItem {
        
        if (_backBtn == nil)
        {
            _backBtn = UIBarButtonItem(image: UIImage(named: "BackIcon")?.withRenderingMode(.alwaysTemplate), style: .plain, target: self, action: nil)
            _backBtn?.tintColor = .primary
        }
        
        return _backBtn!
    }
    var navBarIsVisible: Bool = false
    var navBarIsTransparent: Bool = false
    
    var statusBarStyle: UIStatusBarStyle = .lightContent {
        didSet {
            nav?.statusBarStyle = statusBarStyle
        }
    }
    
    // MARK: Private
    private var hud: JGProgressHUD?
    let disposeBag = DisposeBag()
    
    // MARK: - Methods
    // MARK: Class
    
    
    
    // MARK: Lifecycle
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Settings della status bar
        statusBarStyle = .default
        
        navigationItem.hidesBackButton = true
        if #available(iOS 13.0, *) {
          overrideUserInterfaceStyle = .light
        }
        self.view.backgroundColor = .white
        
        setupUi()
        setupConstraints()
        setupBindings()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // NAVIGATION BAR
        // Impostazione dello stile dei pulsanti testuali e del titolo
        let barButtonAttributes = [
            NSAttributedString.Key.font: UIFont.medium14,
            NSAttributedString.Key.foregroundColor: UIColor.white,
        ]
        let barButtonDisabledAttributes = [
            NSAttributedString.Key.font: UIFont.medium14,
            NSAttributedString.Key.foregroundColor: UIColor.systemGreen,
        ]
        
        nav?.navigationBar.titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.white,
        ]
        
        navigationItem.leftBarButtonItem?.setTitleTextAttributes(barButtonAttributes, for: .normal)
        navigationItem.leftBarButtonItem?.setTitleTextAttributes(barButtonAttributes, for: .highlighted)
        navigationItem.leftBarButtonItem?.setTitleTextAttributes(barButtonDisabledAttributes, for: .disabled)
        navigationItem.rightBarButtonItem?.setTitleTextAttributes(barButtonAttributes, for: .normal)
        navigationItem.rightBarButtonItem?.setTitleTextAttributes(barButtonAttributes, for: .highlighted)
        navigationItem.rightBarButtonItem?.setTitleTextAttributes(barButtonDisabledAttributes, for: .disabled)
        navigationItem.backBarButtonItem?.setTitleTextAttributes(barButtonAttributes, for: .normal)
        navigationItem.backBarButtonItem?.setTitleTextAttributes(barButtonAttributes, for: .highlighted)
        navigationItem.backBarButtonItem?.setTitleTextAttributes(barButtonDisabledAttributes, for: .disabled)
        
        nav?.setNavigationBarHidden(!navBarIsVisible, animated: true)
        nav?.navigationBar.isTranslucent = false// navBarIsTransparent ? true : false
        nav?.navigationBar.barTintColor = .primary
        nav?.navigationBar.backgroundColor = .primary
        nav?.navigationBar.setBackgroundImage(navBarIsTransparent ? UIImage() : nil, for: .default)
        
    }
    
    
    // MARK: Custom accessors
    
    
    // MARK: IBActions
    
    
    // MARK: Public
    
    // Metodo da sovrascrivere nelle classi figlie per l'inizializzazione della UI
    func setupUi() {
        
    }
    
    // Metodo da sovrascrivere nelle classi figlie per l'inizializzazione delle constraints
    func setupConstraints() {
        
    }
    
    // Metodo da sovrascrivere nelle classi figlie per l'inizializzazione del binding
    func setupBindings() {
        
        hudIsVisible.asDriver()
            .skip(1)
            .drive(onNext: { [weak self] hudIsVisible in
                if hudIsVisible {
                    self?.showHud()
                } else {
                    self?.hideHud()
                }
            })
            .disposed(by: disposeBag)
        
    }
    
    // Mostra/nasconde il titolo con un fade
    func fadeLabel(label: UILabel, text: String) {
        UIView.animate(withDuration: 0.200, delay: 0.0, options: .curveEaseOut, animations: {
            label.alpha = 0
            }, completion: { finished in
                label.text = text
                UIView.animate(withDuration: 0.200, delay: 0.0, options: .curveEaseOut, animations: {
                    label.alpha = 1
                })
        })
    }
    
    //mostra un alert di sistema con font e colori customizzati, ha come parametro il titolo e il testo dell'alert. Ha un solo pulsante: ok
    func showPopUp(title: String, body: String) {
        let alert = UIAlertController(title: title, message: body, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
    // Mostra un alert di sistema con font e colori customizzati, ha come parametro titolo, testo e due pulsanti
    func showPopUp(title: String, body: String, defaultAction: String, cancelAction: String) {
        let alert = UIAlertController(title: title, message: body, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: defaultAction, style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: cancelAction, style: .cancel, handler: nil))
        navigationController!.present(alert, animated: true)
    }
    
    //mostra un actionsheet che non ha titolo e descrizione, scritte dorate e un pulsante di dismiss
    func showActionSheet(actions: [UIAlertAction]){
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Annulla", style: .cancel, handler: nil))
        for action in actions {
            actionSheet.addAction(action)
        }
        self.present(actionSheet, animated: true)
    }
    
    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        viewControllerToPresent.modalPresentationStyle = .fullScreen
        super.present(viewControllerToPresent, animated: flag, completion: completion)
    }
    
    private func showHud() {
        guard hud == nil else { return }
        hud = JGProgressHUD(style: .extraLight)
        hud?.show(in: self.view)
    }
    
    private func hideHud() {
        hud?.dismiss(animated: true)
        hud = nil
    }

    // MARK: Protocol conformance

}
