//
//  ContactListViewController.swift
//  sample-application
//
//  Created by Alessandro on 11/10/21.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa
import RxDataSources

class ContactListViewController: BaseViewController {

    // MARK: - Properties
    // MARK: Class
    
    
    // MARK: Public
    
    let viewModel: ContactListViewModelProtocol
    
    
    // MARK: Private
    private var contactsDataSource: RxTableViewSectionedReloadDataSource<SectionModel<String, ContactDto>>!
    private var buttonsVisible = false
 
    // UIFields
    private var contactsTableView = UITableView()
    private let callBtn = UIButton()
    private let cancelBtn = UIButton()
    private let buttonWrapperView = UIView()
    private var buttonWrapperViewBottomCnst: Constraint?
    private var logoutBtn = UIBarButtonItem()
    // Output
    
    // MARK: - Methods
    // MARK: Class
    
    
    // MARK: Lifecycle
    required init?(coder aDecoder: NSCoder) {
        fatalError("ContactListViewController must be initialized from init(viewModel:)")
    }
    
    init(viewModel: ContactListViewModelProtocol) {
        self.viewModel = viewModel
        super.init()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // MARK: Custom accessors
    
    
    // MARK: IBActions
    
    
    
    // MARK: Private
    override func setupUi() {
        self.navBarIsVisible = true
        
        logoutBtn.image = UIImage(named: "logout")
        self.navigationItem.rightBarButtonItem = logoutBtn
        
        contactsTableView.backgroundColor = .clear
        contactsTableView.register(ContactCell.self, forCellReuseIdentifier: ContactCell.reuseId)
        contactsTableView.allowsSelection = true
        contactsTableView.separatorStyle = .singleLine
        contactsTableView.isUserInteractionEnabled = true
        contactsTableView.isScrollEnabled = true
        contactsTableView.rowHeight = 74
        contactsTableView.translatesAutoresizingMaskIntoConstraints = false
        contactsTableView.allowsMultipleSelection = true
        contactsTableView.separatorColor = .primaryDark
        
        //contactsTableView.setContentHuggingPriority(.required, for: .vertical)
        
        contactsDataSource = RxTableViewSectionedReloadDataSource<SectionModel<String, ContactDto>>(configureCell: { (dataSource, tableView, indexPath, item) in
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ContactCell.reuseId, for: indexPath) as? ContactCell else {
                return UITableViewCell(frame: .zero)
            }
            cell.configure(with: item)
            return cell
        }, canEditRowAtIndexPath: {[weak self] _,_ in
            guard let self = self else { return false }
            
            return !self.buttonsVisible
        })
        
        view.addSubview(contactsTableView)
        
        buttonWrapperView.setContentHuggingPriority(.required, for: .vertical)
        buttonWrapperView.setContentCompressionResistancePriority(.required, for: .vertical)
        
        // Bottone di salvataggio
        callBtn.layer.cornerRadius = 20
        callBtn.titleLabel?.font = UIFont.medium15
        callBtn.setTitleColor(.white, for: .normal)
        callBtn.backgroundColor = .primary
        callBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        callBtn.titleLabel?.minimumScaleFactor = 0.75
        callBtn.setTitle("Chiama", for: .normal)
        callBtn.setContentHuggingPriority(.required, for: .vertical)
        callBtn.setContentHuggingPriority(.required, for: .horizontal)
        
        buttonWrapperView.addSubview(callBtn)
        
        // Bottone di annullamento modifiche
        cancelBtn.layer.cornerRadius = 20
        cancelBtn.titleLabel?.font = UIFont.medium15
        cancelBtn.setTitleColor(.lightGray, for: .normal)
        cancelBtn.backgroundColor = .clear
        cancelBtn.layer.borderWidth = 2
        cancelBtn.layer.borderColor = UIColor.lightGray.cgColor
        cancelBtn.titleLabel?.adjustsFontSizeToFitWidth = true
        cancelBtn.setTitle("Annulla", for: .normal)
        cancelBtn.setContentHuggingPriority(.required, for: .vertical)
        cancelBtn.setContentHuggingPriority(.required, for: .horizontal)
        cancelBtn.addTarget(self, action: #selector(cancelSelectionAction), for: .touchUpInside)
        
        buttonWrapperView.addSubview(cancelBtn)
        
        view.addSubview(buttonWrapperView)
    }
    
    
    override func setupBindings() {
        super.setupBindings()
        
        viewModel.isLoading
            .asDriver()
            .drive(hudIsVisible)
            .disposed(by: disposeBag)
        
        viewModel.contactList
            .map{ [SectionModel(model: "contacts", items: $0)]}
            .asObservable()
            .bind(to: contactsTableView.rx.items(dataSource: contactsDataSource))
            .disposed(by: disposeBag)
        
        viewModel.selectedContactList
            .subscribe(onNext: { [weak self] selectedContacts in
                guard let self = self else { return }
                
                if (selectedContacts.count > 0) {
                    self.title = "Contatti selezionati: " + String(selectedContacts.count)
                    if !self.buttonsVisible { self.setSaveButtonsVisibility(isVisible: true, animated: true) }
                } else {
                    self.title = "Contatti"
                    if self.buttonsVisible { self.setSaveButtonsVisibility(isVisible: false, animated: true) }
                    self.deselectAllTableViewRows()
                }
            }).disposed(by: disposeBag)
        
        contactsTableView.rx.modelSelected(ContactDto.self)
            .bind { [weak self] val in
                guard let self = self else { return }
                
                if (self.contactsTableView.indexPathsForSelectedRows != nil && self.contactsTableView.indexPathsForSelectedRows!.count > 4) {
                    self.contactsTableView.deselectRow(at: self.contactsTableView.indexPathsForSelectedRows!.last!, animated: true)
                    
                    self.showPopUp(title: "Limite raggiunto", body: "Puoi selezionare massimo quattro contatti per avviare una videochiamata")
                } else {
                    self.viewModel.addSelectedContact(contact: val)
                }
                //self.viewModel.handleListTap(selectedValue: val)
            }
            .disposed(by: disposeBag)
        
        contactsTableView.rx.modelDeselected(ContactDto.self)
            .bind { [weak self] val in
                guard let self = self else { return }
                
                self.viewModel.removeSelectedContact(contact: val)
            }
            .disposed(by: disposeBag)
        
        contactsTableView.rx
              .itemDeleted
              .subscribe(onNext: { indexPath in
                  self.viewModel.contactList
                      .asObservable()
                      .take(1)
                      .subscribe(onNext: { val in
                          self.viewModel.deleteContact(contact: val[indexPath.item])
                      }).disposed(by: self.disposeBag)
              })
              .disposed(by: disposeBag)
        
        callBtn.rx.tap.throttle(.seconds(1), scheduler: MainScheduler.instance)
            .bind(to: viewModel.callTap)
            .disposed(by: disposeBag)
        
        logoutBtn.rx.tap.throttle(.seconds(1), scheduler: MainScheduler.instance)
            .bind(to: viewModel.logoutTap)
            .disposed(by: disposeBag)
    }
    
    override func setupConstraints() {
        contactsTableView.snp.makeConstraints { (make) in
            make.left.right.top.equalToSuperview()
            make.bottom.equalTo(buttonWrapperView.snp.top)
        }
        
        buttonWrapperView.snp.makeConstraints { (make) in
            
            self.buttonWrapperViewBottomCnst = make.top.equalTo(self.view.snp.bottom).constraint// make.bottom.equalToSuperview().constraint
            make.centerX.equalToSuperview()
            make.height.equalTo(90)
        }
        
        cancelBtn.snp.makeConstraints { (make) in
//            make.top.equalTo(scrollView.snp.bottom).inset(UIConstants.vPadding16)
            make.left.equalToSuperview().offset(18)
            make.right.equalTo(callBtn.snp.left).inset(-18)
            make.bottom.equalToSuperview().inset(32)
            make.top.equalToSuperview().inset(18)
            make.width.equalTo(135)
            make.height.equalTo(40)
        }
        
        callBtn.snp.makeConstraints { (make) in
            make.top.equalTo(cancelBtn.snp.top)
            make.right.equalToSuperview().inset(18)
            make.bottom.equalTo(cancelBtn.snp.bottom)
            make.width.equalTo(cancelBtn.snp.width)
            make.height.equalTo(cancelBtn.snp.height)
        }
        
        setSaveButtonsVisibility(isVisible: false, animated: false)
    }
    
    @objc private func cancelSelectionAction(){
        
        deselectAllTableViewRows()
        
        self.viewModel.removeAllSelectedContacts()
        
        self.setSaveButtonsVisibility(isVisible: false, animated: true)
    }
    
    private func deselectAllTableViewRows() {
        if (self.contactsTableView.indexPathsForSelectedRows != nil && self.contactsTableView.indexPathsForSelectedRows!.count > 0) {
            
            for row in self.contactsTableView.indexPathsForSelectedRows! {
                self.contactsTableView.deselectRow(at: row, animated: true)
            }
        }
    }
    
    func setSaveButtonsVisibility(isVisible: Bool, animated: Bool) {
        buttonsVisible = isVisible
        let updateConstraintCallback = {
            let offset = isVisible ? -self.buttonWrapperView.bounds.height : 0
            self.buttonWrapperViewBottomCnst?.update(offset: offset)
            self.view.layoutIfNeeded()
        }
        if animated {
            UIView.animate(withDuration: 0.2, animations: {
                var buttonWrapperFrame = self.buttonWrapperView.frame
                if isVisible {
                    buttonWrapperFrame.origin.y -= buttonWrapperFrame.size.height
                } else {
                    buttonWrapperFrame.origin.y += buttonWrapperFrame.size.height
                    }
                self.buttonWrapperView.frame = buttonWrapperFrame
            }, completion: { finished in
                updateConstraintCallback()
            })
        } else {
            updateConstraintCallback()
        }
    }

}
