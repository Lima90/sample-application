//
//  BaseNavigationController.swift
//  sample-application
//
//  Created by Alessandro on 09/10/21.
//

import UIKit
import SnapKit

class BaseNavigationController: UINavigationController {
    
    // MARK: - Properties
    // MARK: Class
    
    
    // MARK: Public
    var statusBarStyle: UIStatusBarStyle = .default {
        didSet {
            setNeedsStatusBarAppearanceUpdate()
        }
    }
    
    // MARK: Private
    
    
    // MARK: - Methods
    // MARK: Class
    
    
    // MARK: Lifecycle
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Impostazione view
        view.backgroundColor = .primary
        view.tintColor = .white
        
        if #available(iOS 13.0, *) {
          overrideUserInterfaceStyle = .light
        }
        
        // Impostazione navigation bar
        navigationBar.shadowImage = UIImage() //UIImage(named: "NavBarShadow")
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return statusBarStyle
    }
    
    // MARK: Custom accessors
    
    
    // MARK: IBActions
    
    
    // MARK: Public
    
    // MARK: Private
    
    // MARK: Protocol conformance
    
}
