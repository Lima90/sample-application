//
//  LoginViewController.swift
//  sample-application
//
//  Created by Alessandro on 10/10/21.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa

class LoginViewController: BaseViewController, UITextFieldDelegate {

    // MARK: - Properties
    // MARK: Class
    
    
    // MARK: Public
    
    let viewModel: LoginViewModelProtocol
    
    
    // MARK: Private
    
    var tap: UITapGestureRecognizer?
    var press: UILongPressGestureRecognizer?
    var swipe: UISwipeGestureRecognizer?
    var pan: UIPanGestureRecognizer?
    
    // UIFields
    let scrollView = UIScrollView()
    let fieldView = UIView()
    
    private let titleLbl = UILabel()
    private let usernameLbl = UILabel()
    private let usernameTxt = UITextField()
    private let passwordLbl = UILabel()
    private let passwordTxt = UITextField()
    
    private let loginBtn = UIButton()

    private let flowPadding: CGFloat = 8
    
    // Output
    let usernameTxtValue: Driver<String>
    private let usernameTxtValueBr = BehaviorRelay<String>(value: "")
    
    let passwordTxtValue: Driver<String>
    private let passwordTxtValueBr = BehaviorRelay<String>(value: "")
    
    // MARK: - Methods
    // MARK: Class
    
    
    // MARK: Lifecycle
    required init?(coder aDecoder: NSCoder) {
        fatalError("LoginViewController must be initialized from init(viewModel:)")
    }
    
    init(viewModel: LoginViewModelProtocol) {
        self.viewModel = viewModel
        
        usernameTxtValue = usernameTxtValueBr.asDriver().distinctUntilChanged()
        passwordTxtValue = passwordTxtValueBr.asDriver().distinctUntilChanged()
        
        super.init()
        
        navBarIsVisible = false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        usernameTxt.delegate = self
        passwordTxt.delegate = self
        
        //Looks for single or multiple taps.
        tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        
        press = UILongPressGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        
        swipe = UISwipeGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        
        pan = UIPanGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        tap?.cancelsTouchesInView = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name:UIResponder.keyboardWillShowNotification, object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name:UIResponder.keyboardWillHideNotification, object: nil);
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // MARK: Custom accessors
    
    
    // MARK: IBActions
    
    
    
    // MARK: Private
    override func setupUi() {
        
        self.hideKeyboardWhenTappedAround()
        
        view.backgroundColor = .white
        
        // Scroll View
        scrollView.clipsToBounds = true
        fieldView.clipsToBounds = true
        
        // Welcome message
        titleLbl.text = "Login"
        titleLbl.textAlignment = NSTextAlignment.center
        titleLbl.font = UIFont.medium20
        titleLbl.textColor = UIColor.black
        titleLbl.adjustsFontSizeToFitWidth = true
        titleLbl.minimumScaleFactor = 0.75
        
        // Username label
        usernameLbl.text = "Username"
        usernameLbl.font = UIFont.regular12
        usernameLbl.textColor = UIColor.gray
        usernameLbl.adjustsFontSizeToFitWidth = true
        
        // Email textfield
        usernameTxt.font = UIFont.medium15
        usernameTxt.setBottomBorder()
        usernameTxt.autocapitalizationType = UITextAutocapitalizationType.none
        usernameTxt.keyboardType = .emailAddress
        usernameTxt.autocorrectionType = .no
        if #available(iOS 11.0, *) {
            usernameTxt.textContentType = .username
        }
        usernameTxt.addTarget(self, action: #selector(usernameValidation), for: .allEditingEvents)
                
        // Password label
        passwordLbl.text = "Password"
        passwordLbl.font = UIFont.regular12
        passwordLbl.textColor = UIColor.gray
        passwordLbl.adjustsFontSizeToFitWidth = true
        
        // Password textfield
        passwordTxt.font = UIFont.medium15
        passwordTxt.isSecureTextEntry = true
        passwordTxt.setBottomBorder()
        passwordTxt.autocapitalizationType = UITextAutocapitalizationType.none
        passwordTxt.autocorrectionType = .no
        if #available(iOS 11.0, *) {
            passwordTxt.textContentType = .password
        }
        passwordTxt.addTarget(self, action: #selector(pwdValidation), for: .allEditingEvents)
        
        loginBtn.backgroundColor = .primary
        loginBtn.layer.cornerRadius = 25
        loginBtn.titleLabel?.textColor = .white
        loginBtn.setTitle("Accedi", for: .normal)
        
        // Disposizione
        view.addSubview(titleLbl)
        
        fieldView.addSubview(usernameLbl)
        fieldView.addSubview(usernameTxt)
        fieldView.addSubview(passwordLbl)
        fieldView.addSubview(passwordTxt)
        fieldView.addSubview(loginBtn)
        
        scrollView.addSubview(fieldView)
        
        view.addSubview(scrollView)
        
        formValidation()
    }
    
    @objc func usernameValidation(){
        guard usernameTxt.text != nil else {
            return
        }
        
        formValidation()
        
    }
    
    @objc func pwdValidation(){
        guard passwordTxt.text != nil else {
            return
        }
        
        formValidation()
        
    }
    
    
    @objc func formValidation(){
        guard let username = usernameTxt.text, let password = passwordTxt.text else {
            return
        }
        
        if password.count > 0 && username.count > 0 {
            loginBtn.alpha = 1.0
            loginBtn.isEnabled = true
        }else{
            loginBtn.alpha = 0.5
            loginBtn.isEnabled = false
        }
        
    }
    
    override func setupBindings() {
        super.setupBindings()
        
        // Hud
        viewModel.isLoading
            .asDriver()
            .drive(hudIsVisible)
            .disposed(by: disposeBag)
        
        loginBtn.rx.tap.throttle(.seconds(1), scheduler: MainScheduler.instance)
            .bind(to: viewModel.loginTap)
            .disposed(by: disposeBag)
        
        usernameTxt.rx.controlEvent([.editingDidEndOnExit, .editingDidEnd])
            .withLatestFrom(usernameTxt.rx.text)
            .filterNil()
            .subscribe(onNext: { [weak self] text in
                guard let self = self else { return }
                
                self.usernameTxtValueBr.accept(text)
            })
            .disposed(by: disposeBag)
        
        usernameTxtValue
            .asObservable()
            .bind(to: viewModel.emailTxt)
            .disposed(by: disposeBag)
        
        passwordTxt.rx.controlEvent([.editingDidEndOnExit, .editingDidEnd])
            .withLatestFrom(passwordTxt.rx.text)
            .filterNil()
            .subscribe(onNext: { [weak self] text in
                guard let self = self else { return }
                
                self.passwordTxtValueBr.accept(text)
            })
            .disposed(by: disposeBag)
        
        passwordTxtValue
            .asObservable()
            .bind(to: viewModel.passwordTxt)
            .disposed(by: disposeBag)
        
    }
    
    override func setupConstraints() {
                
        titleLbl.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview().inset(18)
            make.top.equalToSuperview().offset(100)
        }
        
        scrollView.snp.makeConstraints { (make) in
            make.top.equalTo(titleLbl.snp.bottom).offset(18)
            make.left.right.bottom.equalTo(view)
        }
        
        fieldView.snp.makeConstraints { (make) in
            make.top.equalTo(scrollView).offset(18)
            make.left.right.bottom.equalTo(view)
            make.width.equalTo(scrollView)//.priority(.high)
        }
        
        usernameLbl.snp.makeConstraints { (make) in
            make.left.equalToSuperview().inset(18)
            make.top.equalToSuperview().offset(18)
            
        }
        
        usernameTxt.snp.makeConstraints { (make) in
            make.left.equalToSuperview().inset(18)
            make.right.equalToSuperview().inset(18)
            make.top.equalTo(usernameLbl.snp.bottom)
            make.height.equalTo(40)
        }
        
        passwordLbl.snp.makeConstraints { (make) in
            make.left.equalToSuperview().inset(18)
            make.top.equalTo(usernameTxt.snp.bottom).offset(42)
        }
        
        passwordTxt.snp.makeConstraints { (make) in
            make.left.equalToSuperview().inset(18)
            make.right.equalToSuperview().inset(18)
            make.top.equalTo(passwordLbl.snp.bottom)
            make.height.equalTo(40)
        }
        
        loginBtn.snp.makeConstraints { (make) in
            make.left.equalToSuperview().inset(18)
            make.right.equalToSuperview().inset(18)
            make.top.equalTo(passwordTxt.snp.bottom).offset(42)
            make.height.equalTo(50)
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        if let tap = tap, let press = press, let swipe = swipe, let pan = pan {
            view.addGestureRecognizer(tap)
            view.addGestureRecognizer(press)
            view.addGestureRecognizer(swipe)
            view.addGestureRecognizer(pan)
        }
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            self.scrollView.snp.remakeConstraints { (remake) in
                remake.top.equalTo(titleLbl.snp.bottom).offset(18)
                remake.left.right.equalTo(view)
                remake.bottom.equalTo(view).offset(-18 - keyboardSize.height)
            }
            
            var contentInset:UIEdgeInsets = self.scrollView.contentInset
            contentInset.bottom = keyboardSize.height + 84
            scrollView.contentInset = contentInset
               
        }
        
        
        
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        
        if let tap = tap, let press = press, let swipe = swipe, let pan = pan {
            view.removeGestureRecognizer(tap)
            view.removeGestureRecognizer(press)
            view.removeGestureRecognizer(swipe)
            view.removeGestureRecognizer(pan)
        }
        
        self.scrollView.snp.remakeConstraints { (remake) in
            remake.top.equalTo(titleLbl.snp.bottom).offset(18)
            remake.left.right.bottom.equalTo(view)
        }
        
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
        
    }
    
    // MARK: Private
    
    @objc private func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case usernameTxt:
            passwordTxt.becomeFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        return false
    }
    
    // MARK: Protocol conformance

}
