//
//  ConversationCoordinator.swift
//  sample-application
//
//  Created by Alessandro on 13/10/21.
//

import UIKit

protocol ConversationCoordinatorDelegate: AnyObject {
    func conversationCoordinatorDismissed()
}

class ConversationCoordinator: Coordinator {
    
    // MARK: - Properties
    // MARK: Class
    
    
    // MARK: Public
    weak var delegate: ConversationCoordinatorDelegate?
    var children: [Coordinator] = []
    
    var contacts: [ContactDto] = []
    
    // MARK: Private
    let navigationController: BaseNavigationController
    private var innerStack: [UIViewController] = []
    private var convVc: ConversationViewController?
    
    // MARK: - Methods
    // MARK: Class
    
    
    // MARK: Lifecycle
    init(navigationController: BaseNavigationController) {
        self.navigationController = navigationController
    }
    
    // MARK: Custom accessors
    
    
    // MARK: IBActions
    
    
    // MARK: Public
    func start() {
        if contacts.isEmpty {
            fatalError("contacts must be not empty")
        }
        
        // Se lo stack è vuoto
        if !innerStack.isEmpty {
            navigationController.viewControllers = innerStack
        }
        
        var roomService = AppContainer.shared.resolve(RoomServiceProtocol.self)!
        roomService.contactList = contacts
        
        var convVm = AppContainer.shared.resolve(ConversationViewModelProtocol.self)!
        convVc = ConversationViewController(viewModel: convVm)
        convVm.flowDelegate = self
        convVm.roomService = roomService
        
        if #available(iOS 13.0, *) {
            convVc!.isModalInPresentation = true
        }
        
        convVc!.modalPresentationStyle = .fullScreen
        
        navigationController.present(convVc!, animated: true)
    }
    
    // MARK: Protocol conformance
    
}

extension ConversationCoordinator: ConversationFlowDelegate {
    func endConversation() {
        convVc?.dismiss(animated: true, completion: { [weak self] in
            guard let self = self else { return }
            self.delegate?.conversationCoordinatorDismissed()
        })
    }
}
