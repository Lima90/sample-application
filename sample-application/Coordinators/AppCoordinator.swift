//
//  AppCoordinator.swift
//  sample-application
//
//  Created by Alessandro on 09/10/21.
//

import Swinject
import RxSwift
import JGProgressHUD

class AppCoordinator: Coordinator {
    // MARK: - Properties
    // MARK: Class
    
    
    // MARK: Public
    var children: [Coordinator] = []
    
    // MARK: Private
    private let disposeBag = DisposeBag()
    private let navigationController: BaseNavigationController
    private let loginCoordinator: LoginCoordinator
    private let contactListCoordinator: ContactListCoordinator
    private let conversationCoordinator: ConversationCoordinator
    
    private var hud: JGProgressHUD?

    // MARK: - Methods
    // MARK: Class
    
    
    // MARK: Lifecycle
    init(navigationController: BaseNavigationController) {
        self.navigationController = navigationController
        
        loginCoordinator = LoginCoordinator(navigationController: navigationController)
        contactListCoordinator = ContactListCoordinator(navigationController: navigationController)
        conversationCoordinator = ConversationCoordinator(navigationController: navigationController)
        
        setupBindings()
    }
    
    // MARK: Custom accessors
    
    
    // MARK: IBActions
    
    
    // MARK: Public
    
    func start() {
//        var cacheService = AppContainer.shared.resolve(CacheServiceProtocol.self)!
//        cacheService.auth = nil // TODO rimuovere quando implemento logout
        showNextPage()
    }
    
    func showAlert(_ alert: UIAlertController) {
        navigationController.present(alert, animated: true, completion: nil)
    }
    
    func showHud() {
        guard hud == nil else { return }
        hud = JGProgressHUD(style: .extraLight)
        hud?.show(in: self.navigationController.view)
    }
    
    func hideHud() {
        hud?.dismiss(animated: true)
        hud = nil
    }
    
    // MARK: Private
    private func setupBindings() {
        
    }
    
    private func showNextPage() {
        let cacheService = AppContainer.shared.resolve(CacheServiceProtocol.self)!
        if (cacheService.auth != nil) {
            add(child: contactListCoordinator)
            contactListCoordinator.delegate = self
            contactListCoordinator.start()
        } else {
            add(child: loginCoordinator)
            loginCoordinator.delegate = self
            loginCoordinator.start()
        }
    }
    
    // MARK: Protocol conformance
    
}


// MARK: - LoginCoordinatorDelegate
extension AppCoordinator: LoginCoordinatorDelegate {
    func loginCoordinatorDismissed() {
        showNextPage()
        remove(child: self.loginCoordinator)
    }
}

// MARK: - ContactListCoordinatorDelegate
extension AppCoordinator: ContactListCoordinatorDelegate {
    func startConversation(contactToCall: [ContactDto]) {
        conversationCoordinator.contacts = contactToCall
        conversationCoordinator.start()
    }
    
    func allCoordinatorDismissed() {
        showNextPage()
        remove(child: self.contactListCoordinator)
    }
}

// MARK: - ContactListCoordinatorDelegate
extension AppCoordinator: ConversationCoordinatorDelegate {
    func conversationCoordinatorDismissed() {
        remove(child: self.loginCoordinator)
    }
}
