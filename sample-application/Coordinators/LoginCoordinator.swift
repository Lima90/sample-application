//
//  LoginCoordinator.swift
//  sample-application
//
//  Created by Alessandro on 11/10/21.
//

import UIKit

protocol LoginCoordinatorDelegate: AnyObject {
    func loginCoordinatorDismissed()
}

class LoginCoordinator: Coordinator {
    
    // MARK: - Properties
    // MARK: Class
    
    
    // MARK: Public
    weak var delegate: LoginCoordinatorDelegate?
    var children: [Coordinator] = []
    
    // MARK: Private
    let navigationController: BaseNavigationController
    private var innerStack: [UIViewController] = []
    
    // MARK: - Methods
    // MARK: Class
    
    
    // MARK: Lifecycle
    init(navigationController: BaseNavigationController) {
        self.navigationController = navigationController
    }
    
    // MARK: Custom accessors
    
    
    // MARK: IBActions
    
    
    // MARK: Public
    func start() {
        // Se lo stack è vuoto
        if !innerStack.isEmpty {
            navigationController.viewControllers = innerStack
        }
        
        var loginVm = AppContainer.shared.resolve(LoginViewModelProtocol.self)!
        let loginVc = LoginViewController(viewModel: loginVm)
        loginVm.flowDelegate = self
        navigationController.viewControllers = [loginVc]
    }
    
    // MARK: Protocol conformance
    
}

extension LoginCoordinator: LoginFlowDelegate {
    func loginSuccess() {
        delegate?.loginCoordinatorDismissed()
    }
}
