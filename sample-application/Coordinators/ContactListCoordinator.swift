//
//  ContactListCoordinator.swift
//  sample-application
//
//  Created by Alessandro on 11/10/21.
//

import UIKit

protocol ContactListCoordinatorDelegate: AnyObject {
    func startConversation(contactToCall: [ContactDto])
    func allCoordinatorDismissed()
}

class ContactListCoordinator: Coordinator {
    
    // MARK: - Properties
    // MARK: Class
    
    
    // MARK: Public
    weak var delegate: ContactListCoordinatorDelegate?
    var children: [Coordinator] = []
    
    // MARK: Private
    let navigationController: BaseNavigationController
    private var innerStack: [UIViewController] = []
    
    // MARK: - Methods
    // MARK: Class
    
    
    // MARK: Lifecycle
    init(navigationController: BaseNavigationController) {
        self.navigationController = navigationController
    }
    
    // MARK: Custom accessors
    
    
    // MARK: IBActions
    
    
    // MARK: Public
    func start() {
        // Se lo stack è vuoto
        if !innerStack.isEmpty {
            navigationController.viewControllers = innerStack
        }
        
        var contactVm = AppContainer.shared.resolve(ContactListViewModelProtocol.self)!
        let contactVc = ContactListViewController(viewModel: contactVm)
        contactVm.flowDelegate = self
        navigationController.viewControllers = [contactVc]
    }
    
    // MARK: Protocol conformance
    
}

extension ContactListCoordinator: ContactListFlowDelegate {
    
    func startConversation(contactToCall: [ContactDto]) {
        delegate?.startConversation(contactToCall: contactToCall)
    }
    
    func logoutSuccess() {
        delegate?.allCoordinatorDismissed()
    }
}
