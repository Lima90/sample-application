//
//  RoomServiceProtocol.swift
//  sample-application
//
//  Created by Alessandro on 13/10/21.
//

import Foundation
import RxSwift
import RxCocoa

protocol RoomServiceProtocol {
    
    // MARK: General
    var contactList: [ContactDto] { get set }
    
    // MARK: Connection
    var connectionState: Observable<RoomConnectionState> { get }
    func connect() -> Void
    func disconnect() -> Void
    
    // MARK: Conversation
    var connectedStreams: Observable<[ConversationStream]> { get }
    
    // MARK: Outputs
    var micOn: BehaviorRelay<Bool> { get }
    var camOn: BehaviorRelay<Bool> { get }
    
    func changeMicState() -> Void
    func changeCamState() -> Void
}

public enum RoomConnectionState {
    case disconnected
    case connecting
    case connected
}
