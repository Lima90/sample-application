//
//  RoomService.swift
//  sample-application
//
//  Created by Alessandro on 13/10/21.
//

import RxSwift
import RxCocoa
import Darwin

class RoomService: RoomServiceProtocol {
    
    // MARK: General
    var contactList: [ContactDto] = []
    
    init() {
        connectionState = connectionStateBr.asObservable()
        connectedStreams = connectedStreamsBr.asObservable()
        
        micOn = BehaviorRelay<Bool>(value: _micOn)
        camOn = BehaviorRelay<Bool>(value: _camOn)
    }
    
    // MARK: Connection
    var connectionState: Observable<RoomConnectionState>
    
    //MARK: Private
    private let connectionStateBr = BehaviorRelay<RoomConnectionState>(value: .disconnected)
    private let connectedStreamsBr = BehaviorRelay<[ConversationStream]>(value: [])
    
    func connect() -> Void {
        
        self.connectionStateBr.accept(.connecting)
        
        _micOn = true
        micOn.accept(_micOn)
        _camOn = true
        camOn.accept(_camOn)
        
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else { return }

            sleep(2)
            self.connectionStateBr.accept(.connected)

            if (self.contactList.isEmpty) {
                fatalError("contactList must be not empty to start a conversation!")
            }

            //Simulo un ordine casuale di connessione alla chiamata
            self.contactList.shuffle()
            sleep(1)
            for contact in self.contactList {
                sleep(1)
                let stream = ConversationStream(contact: contact)

                var array = self.connectedStreamsBr.value
                array.append(stream)

                self.connectedStreamsBr.accept(array)
            }
        }
    }
    
    func disconnect() -> Void {
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self else { return }
            sleep(1)
            self.connectedStreamsBr.accept([])
            self.connectionStateBr.accept(.disconnected)
        }
    }
    
    // MARK: Conversation
    var connectedStreams: Observable<[ConversationStream]>
    
    // MARK: Outputs
    var micOn: BehaviorRelay<Bool>
    var camOn: BehaviorRelay<Bool>
    
    var _micOn = true
    func changeMicState() -> Void {
        _micOn = !_micOn
        micOn.accept(_micOn)
    }
    
    var _camOn = true
    func changeCamState() -> Void {
        _camOn = !_camOn
        camOn.accept(_camOn)
    }
}
