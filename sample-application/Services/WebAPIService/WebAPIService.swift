//
//  WebAPIService.swift
//  sample-application
//
//  Created by Alessandro on 10/10/21.
//

import Foundation
import RxSwift
import RxOptional


class WebAPIService: WebAPIServiceProtocol {
    // MARK: - Properties
    // MARK: Class
    
    
    // MARK: Public
    
    
    // MARK: Private
    private var cacheService: CacheServiceProtocol
    private let dataService: DataServiceProtocol
    
    
    // MARK: - Methods
    // MARK: Class
    
    
    // MARK: Lifecycle
    init(cacheService: CacheServiceProtocol, dataService: DataServiceProtocol) {
        self.cacheService = cacheService
        self.dataService = dataService
    }
    
    //MARK: Public
    //MARK: Login
    func doLogin(with credentials: CredentialsDto) -> Observable<AuthDto> {
        return Observable.create { observer in
            
            DispatchQueue.global(qos: .userInitiated).async {
                
                sleep(1)//Simulo tempo di attesa risposta API
                
                if (credentials.username == "test" && credentials.password == "12345") {
                    let auth = AuthDto(userId: "1", username: credentials.username)
                    
                    observer.onNext(auth)
                    observer.onCompleted()
                } else {
                    observer.onError(NSError())
                }
            }
            
            
            return Disposables.create()
        }
    }
    
    func doLogout(with auth: AuthDto?) -> Observable<Void> {
        return Observable.create { observer in
            
            DispatchQueue.global(qos: .userInitiated).async {
                
                sleep(1)//Simulo tempo di attesa risposta API
                
                observer.onNext(())
                observer.onCompleted()
            }
            
            
            return Disposables.create()
        }
    }
}
