//
//  WebAPIServiceProtocol.swift
//  sample-application
//
//  Created by Alessandro on 10/10/21.
//

import Foundation
import RxSwift

protocol WebAPIServiceProtocol {
    //MARK: Login
    func doLogin(with credentials: CredentialsDto) -> Observable<AuthDto>
    func doLogout(with auth: AuthDto?) -> Observable<Void>
}
