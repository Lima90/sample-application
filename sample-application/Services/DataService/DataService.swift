//
//  DataService.swift
//  sample-application
//
//  Created by Alessandro on 10/10/21.
//

import RxSwift
import RxCocoa

class DataService: DataServiceProtocol {
    // MARK: Private
    private var cacheService: CacheServiceProtocol
    private var rawContacts: [ContactDto] = []
    
    // MARK: Lifecycle
    init(cacheService: CacheServiceProtocol) {
        self.cacheService = cacheService
        
        //Moked Data
        rawContacts.append(ContactDto(name: "Mario", surname: "Rossi"))
        rawContacts.append(ContactDto(name: "Giuseppe", surname: "Verdi"))
        rawContacts.append(ContactDto(name: "Gianluca", surname: "Gialli"))
        rawContacts.append(ContactDto(name: "Giuseppina", surname: "Bianchi"))
        rawContacts.append(ContactDto(name: "Angelo", surname: "Alberti"))
        rawContacts.append(ContactDto(name: "Marco", surname: "Genovesi"))
        rawContacts.append(ContactDto(name: "Maria", surname: "Brambilla"))
        rawContacts.append(ContactDto(name: "Alfonso", surname: "Capitone"))
        rawContacts.append(ContactDto(name: "Matteo", surname: "Scotti"))
        rawContacts.append(ContactDto(name: "Giulia", surname: "Fumagalli"))
        rawContacts.append(ContactDto(name: "Michela", surname: "Rusnati"))
        rawContacts.append(ContactDto(name: "Francesco", surname: "Mattei"))
        rawContacts.append(ContactDto(name: "Luca", surname: "Bertoni"))
        rawContacts.append(ContactDto(name: "Alessandro", surname: "Limardo"))
        rawContacts.append(ContactDto(name: "Silvio", surname: "Landini"))
        rawContacts.append(ContactDto(name: "Giovanni", surname: "Castagna"))
        rawContacts.append(ContactDto(name: "Michele", surname: "Visentin"))
        rawContacts.append(ContactDto(name: "Marco", surname: "Berenghi"))
    }
    
    //MARK: Public
    // MARK: Contacts
    func allContacts() -> Observable<[ContactDto]> {
        return Observable.of(rawContacts.sorted { $0.surname < $1.surname })
    }
    
    func deleteContact(contact: ContactDto) -> Void {
        rawContacts = rawContacts.filter({ $0 != contact })
    }
}
