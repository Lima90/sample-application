//
//  DataServiceProtocol.swift
//  sample-application
//
//  Created by Alessandro on 10/10/21.
//

import RxSwift

protocol DataServiceProtocol {
    // MARK: Contacts
    func allContacts() -> Observable<[ContactDto]>
    func deleteContact(contact: ContactDto) -> Void
}
