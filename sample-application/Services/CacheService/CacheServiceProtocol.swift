//
//  CacheServiceProtocol.swift
//  sample-application
//
//  Created by Alessandro on 10/10/21.
//

import RxSwift

protocol CacheServiceProtocol {
    
    // MARK: Account
    var auth: AuthDto? { get set }
    
    // MARK: General
    func deleteCache()
    
    // MARK: Images
    func imageFromCache(urlString: String) -> UIImage?
    func cacheImage(image: UIImage?, urlString: String) -> Void
    func deleteCachedImage(urlString: String) -> Void
    func deleteAllImageCache() -> Void
}

