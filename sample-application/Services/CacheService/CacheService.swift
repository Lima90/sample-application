//
//  CacheService.swift
//  sample-application
//
//  Created by Alessandro on 10/10/21.
//

import RxSwift
import RxCocoa
import KeychainAccess

class CacheService: CacheServiceProtocol {
    // MARK: - Properties
    // MARK: Class
    static let resetKeychainDataOnFirstLaunchKey = "sample_cache_service_first_launch_key"
    static let keychainService: String = "sample_keychain"
    static let keychainAuthKey: String = "sample_auth_key"
    // MARK: Private
    private let imageCache = NSCache<AnyObject, AnyObject>()
    
    // MARK: Public
    // MARK: Account
    internal var _auth: AuthDto?
    var auth: AuthDto? {
        get {
          if _auth == nil {
            if let jsonString = keychain()[CacheService.keychainAuthKey] {
              _auth = AuthDto(JSONString: jsonString)
            }
          }
          return _auth
        }
        set {
          _auth = newValue
          if let auth = newValue {
            let jsonString = auth.toJSONString(prettyPrint: false)
            keychain()[CacheService.keychainAuthKey] = jsonString
          } else {
            keychain()[CacheService.keychainAuthKey] = nil
          }
        }
    }
    
    // MARK: Lifecycle
    init() {
      let resetKeychainDataOnFirstLaunchAlreadyDone = UserDefaults.standard.bool(forKey: CacheService.resetKeychainDataOnFirstLaunchKey)
      if !resetKeychainDataOnFirstLaunchAlreadyDone {
        UserDefaults.standard.set(true, forKey: CacheService.resetKeychainDataOnFirstLaunchKey)
      }
    }
    
    
    // MARK: Private
    private func keychain() -> Keychain {
      return Keychain(service: CacheService.keychainService)
    }
    
    
    // MARK: General
    
    func deleteCache() {
        
    }
    
    // MARK: Images
    func imageFromCache(urlString: String) -> UIImage? {
        if let imageFromCache = imageCache.object(forKey: urlString as AnyObject) as? UIImage{
            return imageFromCache
        }
        return nil
    }
    
    func cacheImage(image: UIImage?, urlString: String) {
        if (image != nil) {
            imageCache.setObject(image!, forKey: urlString as AnyObject)
        } else {
            imageCache.removeObject(forKey: urlString as AnyObject)
        }
    }
    
    func deleteCachedImage(urlString: String) {
        cacheImage(image: nil, urlString: urlString)
    }
    
    func deleteAllImageCache() {
        imageCache.removeAllObjects()
    }
}
