//
//  AppContainer.swift
//  sample-application
//
//  Created by Alessandro on 10/10/21.
//

import Swinject
import RxSwift

class AppContainer {

    static let shared: Container = {
        
        let container = Container()
        
        //************* HELPERS **************//
        
        //************* SERVICES *************//
        
        // DataService
        container.register(DataServiceProtocol.self) { (r: Resolver) in
            let cacheService = r.resolve(CacheServiceProtocol.self)!
            return DataService(cacheService: cacheService)
        }.inObjectScope(.container)
        
        // CacheService
        container.register(CacheServiceProtocol.self) { _ in
            return CacheService()
        }.inObjectScope(.container)
        
        // WebAPIService
        container.register(WebAPIServiceProtocol.self) { (r: Resolver) in
            let cacheService = r.resolve(CacheServiceProtocol.self)!
            let dataService = r.resolve(DataServiceProtocol.self)!
            return WebAPIService(cacheService: cacheService, dataService: dataService)
        }
        
        // RoomService
        container.register(RoomServiceProtocol.self) { _ in
            return RoomService()
        }.inObjectScope(.container)
        
        //************* VIEWMODELS *************//
        
        //Login
        container.register(LoginViewModelProtocol.self) { _ in
            LoginViewModel(container: container)
        }
        
        //Contacts
        container.register(ContactListViewModelProtocol.self) { _ in
            ContactListViewModel(container: container)
        }
        
        //Conversation
        container.register(ConversationViewModelProtocol.self) { _ in
            ConversationViewModel(container: container)
        }
        
        return container
    }()
}
