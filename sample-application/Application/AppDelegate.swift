//
//  AppDelegate.swift
//  sample-application
//
//  Created by Alessandro on 08/10/21.
//

import UIKit
import RxSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var appCoordinator: Coordinator?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // Impostazione del coordinator principale
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.tintColor = .primary
        window?.makeKeyAndVisible()
        let appNavigationController = BaseNavigationController()
        window?.rootViewController = appNavigationController
        appCoordinator = AppCoordinator(navigationController: appNavigationController)
        
        appCoordinator?.start()
        
        return true
    }
}

