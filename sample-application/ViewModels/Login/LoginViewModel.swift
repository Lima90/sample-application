//
//  LoginViewModel.swift
//  sample-application
//
//  Created by Alessandro on 10/10/21.
//

import Swinject
import RxSwift
import RxCocoa

protocol LoginFlowDelegate: AnyObject {
    func loginSuccess()
}

protocol LoginViewModelProtocol: BaseViewModelProtocol {
    
    // Flowh
    var flowDelegate: LoginFlowDelegate? { get set }
    
    // Outputs
    
    // Inputs
    var loginTap: AnyObserver<Void> { get }
    
    var emailTxt: AnyObserver<String> { get }
    var passwordTxt: AnyObserver<String> { get }
    
    // Bidirectionals
}

class LoginViewModel: BaseViewModel, LoginViewModelProtocol {
    
    // MARK: - Properties
    // MARK: Class
    
    
    // MARK: Public
    //Flow
    weak var flowDelegate: LoginFlowDelegate?
    
    //Inputs
    let loginTap: AnyObserver<Void>
    
    let emailTxt: AnyObserver<String>
    let passwordTxt: AnyObserver<String>
    
    // MARK: Private
    private let disposeBag = DisposeBag()
    
    // MARK: - Methods
    // MARK: Class
    
    
    // MARK: Lifecycle
    override init(container: Container) {
        
        // Inputs
        let loginTapPs = PublishSubject<Void>()
        loginTap = loginTapPs.asObserver()
        
        let emailTxtPs = PublishSubject<String>()
        emailTxt = emailTxtPs.asObserver()
        
        let passwordTxtPs = PublishSubject<String>()
        passwordTxt = passwordTxtPs.asObserver()
        
        super.init(container: container)
        

        
        loginTapPs.asObservable()
            .withLatestFrom(Observable.combineLatest(emailTxtPs, passwordTxtPs))
            .subscribe(onNext: { [weak self] username, password in
                guard let self = self else { return }
                
                self.isLoading.accept(true)
                
                let body = CredentialsDto(username: username, password: password)
                
                self.webAPIService.doLogin(with: body)
                    .observeOn(MainScheduler.instance)
                    .subscribe(onNext: { [weak self] auth in
                        guard let self = self else { return }
                        self.isLoading.accept(false)
                        guard let _ = auth.userId, let _ = auth.username else {
                            BaseViewModel.displayAlert(title: "Errore", text: "Si è verificato un errore sconosciuto", cancelText: "Ok")
                            return
                        }
                        self.cacheService.auth = auth
                        self.flowDelegate?.loginSuccess()
                        
                    }, onError: { error in
                        
                        self.isLoading.accept(false)
                        // mostro l'errore all'utente
                        BaseViewModel.displayAlert(title: "Login", text: "Username e/o password errati", cancelText: "Ok")
                    })
                    .disposed(by: self.disposeBag)
            })
            .disposed(by: self.disposeBag)
            
    }
    
    // MARK: Custom accessors
    
    
    // MARK: IBActions
    
    
    // MARK: Public
    
}
