//
//  BaseViewModel.swift
//  sample-application
//
//  Created by Alessandro on 10/10/21.
//

import UIKit
import RxSwift
import RxCocoa
import Swinject
import JGProgressHUD

protocol BaseViewModelProtocol {
    var isLoading: BehaviorRelay<Bool> { get }
}

class BaseViewModel: BaseViewModelProtocol {
    
    // Outputs
    let isLoading: BehaviorRelay<Bool>
    
    // Services
    public let container: Container
    
    public lazy var dataService: DataServiceProtocol = {
        return self.container.resolve(DataServiceProtocol.self)!
    }()
    
    public lazy var cacheService: CacheServiceProtocol = {
        return self.container.resolve(CacheServiceProtocol.self)!
    }()
    
    public lazy var webAPIService: WebAPIServiceProtocol = {
        return self.container.resolve(WebAPIServiceProtocol.self)!
    }()
    
    // MARK: Private
    private let disposeBag = DisposeBag()
    
    // MARK: Lifecycle
    init(container: Container) {
        
        self.container = container
        isLoading = BehaviorRelay<Bool>(value: false)
        
    }
    
    //MARK: Static Members
    
    public static func displayAlert(title: String, text: String, cancelText: String) {
        let alert = BaseViewModel.createAlert(title, text, cancel: cancelText)
            
        BaseViewModel.showAlert(alert: alert, animated: true)
    }
    
    public static func displayAlert(title: String, text: String, confirmText: String, cancelText: String, onOk: ((UIAlertAction) -> Void)? = nil, onCancel: ((UIAlertAction) -> Void)? = nil) {
        let alert = BaseViewModel.createAlert(title, text, confirm: confirmText, cancel: cancelText, onOk, onCancel)
            
        BaseViewModel.showAlert(alert: alert, animated: true)
    }
    
    public static func displayAlert(title: String, text: String, confirmText: String, onOk: ((UIAlertAction) -> Void)? = nil) {
        let alert = BaseViewModel.createAlert(title, text, confirm: confirmText, onOk)
            
        BaseViewModel.showAlert(alert: alert, animated: true)
    }
    
    static func createAlert(_ title: String, _ message: String, cancel: String) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: cancel, style: .cancel))
        return alert
    }
    
    // Crea un alert con due pulsanti
    static func createAlert(_ title: String, _ message: String, confirm: String, cancel: String, _ confirmHandler: ((UIAlertAction) -> Void)? = nil, _ cancelHandler: ((UIAlertAction) -> Void)? = nil) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: confirm, style: .default, handler: confirmHandler))
        alert.addAction(UIAlertAction(title: cancel, style: .cancel, handler: cancelHandler))
        return alert
    }
    
    // Crea un alert con un pulsante di conferma e basta
    static func createAlert(_ title: String, _ message: String, confirm: String, _ confirmHandler: ((UIAlertAction) -> Void)? = nil) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: confirm, style: .default, handler: confirmHandler))
        return alert
    }
    
    // MARK: Private
    
    private static func showAlert(alert: UIAlertController, animated: Bool) {
        ((UIApplication.shared.delegate as! AppDelegate).window?.rootViewController as! BaseNavigationController).present(alert, animated: animated)
    }
}
