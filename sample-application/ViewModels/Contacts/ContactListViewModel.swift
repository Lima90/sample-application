//
//  ContactListViewModel.swift
//  sample-application
//
//  Created by Alessandro on 11/10/21.
//

import Swinject
import RxSwift
import RxCocoa

protocol ContactListFlowDelegate: AnyObject {
    func startConversation(contactToCall: [ContactDto])
    func logoutSuccess()
}

protocol ContactListViewModelProtocol: BaseViewModelProtocol {
    
    // Flow
    var flowDelegate: ContactListFlowDelegate? { get set }
    
    // Outputs
    
    var contactList: Driver<[ContactDto]> { get }
    var selectedContactList: Observable<[ContactDto]> { get }
    
    // Inputs
    var callTap: AnyObserver<Void> { get }
    var logoutTap: AnyObserver<Void> { get }
    
    // Public
    func deleteContact(contact: ContactDto) -> Void
    func addSelectedContact(contact: ContactDto) -> Void
    func removeSelectedContact(contact: ContactDto) -> Void
    func removeAllSelectedContacts() -> Void
    // Bidirectionals
}

class ContactListViewModel: BaseViewModel, ContactListViewModelProtocol {
    
    // MARK: - Properties
    // MARK: Class
    
    
    // MARK: Public
    //Flow
    weak var flowDelegate: ContactListFlowDelegate?
    
    // Outputs
    
    var contactList: Driver<[ContactDto]>
    var selectedContactList: Observable<[ContactDto]>
    
    //Inputs
    let callTap: AnyObserver<Void>
    var logoutTap: AnyObserver<Void>
    
    // MARK: Private
    private let disposeBag = DisposeBag()
    private let contactListBr = BehaviorRelay<[ContactDto]>(value: [])
    private let selectedContactRawList = BehaviorRelay<[ContactDto]>(value: [])
    // MARK: - Methods
    // MARK: Class
    
    
    // MARK: Lifecycle
    override init(container: Container) {
        // Outputs
        contactList = contactListBr.asDriver()
        selectedContactList = selectedContactRawList.asObservable()
        // Inputs
        let callTapPs = PublishSubject<Void>()
        callTap = callTapPs.asObserver()
        
        let logoutTapPs = PublishSubject<Void>()
        logoutTap = logoutTapPs.asObserver()
        
        super.init(container: container)
        
        callTapPs.asObservable()
            .withLatestFrom(selectedContactList)
            .subscribe(onNext: { [weak self] selCont in
                guard let self = self else { return }
                
                if selCont.count > 0 && selCont.count <= 4 {
                    self.flowDelegate?.startConversation(contactToCall: selCont)
                    self.removeAllSelectedContacts()
                } else if selCont.count > 0 {
                    BaseViewModel.displayAlert(title: "Errore", text: "Selezionare massimo quattro contatti per avviare la chiamata", cancelText: "Ok")
                } else {
                    BaseViewModel.displayAlert(title: "Errore", text: "Selezionare almeno un contatto per avviare la chiamata", cancelText: "Ok")
                }
            })
            .disposed(by: self.disposeBag)
        
        logoutTapPs.asObservable()
            .subscribe(onNext: { auth in
                
                BaseViewModel.displayAlert(title: "Logout", text: "Sei sicuro di voler eseguire il logout?", confirmText: "Logout", cancelText: "Annulla", onOk: { [weak self] _ in
                    guard let self = self else { return }
                    
                    self.isLoading.accept(true)
                    
                    self.webAPIService.doLogout(with: self.cacheService.auth)
                        .observeOn(MainScheduler.instance)
                        .subscribe(onNext: { [weak self] auth in
                            guard let self = self else { return }
                            self.isLoading.accept(false)
                            
                            self.cacheService.auth = nil
                            self.flowDelegate?.logoutSuccess()
                            
                        }, onError: { error in
                            
                            self.isLoading.accept(false)
                            // mostro l'errore all'utente
                            BaseViewModel.displayAlert(title: "Logout", text: "Si è verificato un errore sconosciuto, riprova più tardi.", cancelText: "Ok")
                        })
                        .disposed(by: self.disposeBag)
                    
                }, onCancel: nil)
            })
            .disposed(by: self.disposeBag)
        
        self.refreshContactList()
    }
    
    // MARK: Custom accessors
    
    
    // MARK: IBActions
    
    
    // MARK: Public
    func deleteContact(contact: ContactDto) -> Void {
        BaseViewModel.displayAlert(title: contact.name + " " + contact.surname, text: "Sei sicuro di voler cancellare questo contatto?", confirmText: "Cancellalo", cancelText: "Annulla", onOk: { [weak self] _ in
            guard let self = self else { return }
            
            self.dataService.deleteContact(contact: contact)
            self.refreshContactList()
            
        }, onCancel: nil)
    }
    
    func addSelectedContact(contact: ContactDto) -> Void {
        var array = selectedContactRawList.value
        array.append(contact)
        selectedContactRawList.accept(array)
    }
    
    func removeSelectedContact(contact: ContactDto) -> Void {
        var array = selectedContactRawList.value
        array = array.filter({ $0 != contact })
        selectedContactRawList.accept(array)
    }
    
    func removeAllSelectedContacts() -> Void {
        selectedContactRawList.accept([])
    }
    
    // MARK: Private
    private func refreshContactList() -> Void {
        self.dataService.allContacts()
            .bind(to: self.contactListBr)
            .disposed(by: self.disposeBag)
    }
}
