//
//  ConversationViewModel.swift
//  sample-application
//
//  Created by Alessandro on 13/10/21.
//

import Swinject
import RxSwift
import RxCocoa

protocol ConversationFlowDelegate: AnyObject {
    func endConversation()
}

protocol ConversationViewModelProtocol: BaseViewModelProtocol {
    
    // MARK: - Properties
    // MARK: Conversation
    var roomService: RoomServiceProtocol? { get set }
    // MARK: Flow
    var flowDelegate: ConversationFlowDelegate? { get set }
    
    // Outputs
    
    // Inputs
    var micTap: AnyObserver<Void> { get }
    var camTap: AnyObserver<Void> { get }
    var hangUpTap: AnyObserver<Void> { get }
    // Public
//    func setupBindings() -> Void
    
    // Bidirectionals
}

class ConversationViewModel: BaseViewModel, ConversationViewModelProtocol {
    
    // MARK: - Properties
    // MARK: Conversation
    var roomService: RoomServiceProtocol?
    
    // MARK: Public
    //Flow
    weak var flowDelegate: ConversationFlowDelegate?
    
    // Outputs
    
    
    //Inputs
    var micTap: AnyObserver<Void>
    var camTap: AnyObserver<Void>
    var hangUpTap: AnyObserver<Void>
    
    // MARK: Private
    private let disposeBag = DisposeBag()

    // MARK: - Methods
    // MARK: Class
    
    
    // MARK: Lifecycle
    override init(container: Container) {
        
        // Inputs
        let micTapPs = PublishSubject<Void>()
        micTap = micTapPs.asObserver()
        
        let camTapPs = PublishSubject<Void>()
        camTap = camTapPs.asObserver()
        
        let hangUpTapPs = PublishSubject<Void>()
        hangUpTap = hangUpTapPs.asObserver()
        
        super.init(container: container)
        
        
        micTapPs.asObservable()
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                
                self.roomService?.changeMicState()
            })
            .disposed(by: self.disposeBag)
        
        camTapPs.asObservable()
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                
                self.roomService?.changeCamState()
            })
            .disposed(by: self.disposeBag)
        
        hangUpTapPs.asObservable()
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                
                self.roomService?.disconnect()
                self.flowDelegate?.endConversation()
            })
            .disposed(by: self.disposeBag)
        
    }
    
    // MARK: Custom accessors
    
    // MARK: IBActions
    
    // MARK: Public
    
    // MARK: Private
    
}
