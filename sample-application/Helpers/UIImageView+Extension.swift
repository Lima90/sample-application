//
//  UIImageView+Extension.swift
//  sample-application
//
//  Created by Alessandro on 12/10/21.
//

import Foundation
import UIKit

extension UIImageView {
    
    func load(urlString: String) {
        let cacheService = AppContainer.shared.resolve(CacheServiceProtocol.self)!
        
        if let imageFromCache = cacheService.imageFromCache(urlString: urlString) {
            self.image = imageFromCache
            return
        }
        
        let url = URL(string: urlString)!
        
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        cacheService.cacheImage(image: image, urlString: urlString)
                        self?.image = image
                    }
                }
            }
        }
    }
}
