//
//  UIColor+Extension.swift
//  sample-application
//
//  Created by Alessandro on 09/10/21.
//

import UIKit

// Palette App
extension UIColor {
    
    convenience init?(hex: String) {
        var hexSanitized = hex.trimmingCharacters(in: .whitespacesAndNewlines)
        hexSanitized = hexSanitized.replacingOccurrences(of: "#", with: "")

        var rgb: UInt32 = 0

        var r: CGFloat = 0.0
        var g: CGFloat = 0.0
        var b: CGFloat = 0.0
        var a: CGFloat = 1.0

        let length = hexSanitized.count

        guard Scanner(string: hexSanitized).scanHexInt32(&rgb) else { return nil }

        if length == 6 {
            r = CGFloat((rgb & 0xFF0000) >> 16) / 255.0
            g = CGFloat((rgb & 0x00FF00) >> 8) / 255.0
            b = CGFloat(rgb & 0x0000FF) / 255.0

        } else if length == 8 {
            r = CGFloat((rgb & 0xFF000000) >> 24) / 255.0
            g = CGFloat((rgb & 0x00FF0000) >> 16) / 255.0
            b = CGFloat((rgb & 0x0000FF00) >> 8) / 255.0
            a = CGFloat(rgb & 0x000000FF) / 255.0

        } else {
            return nil
        }

        self.init(red: r, green: g, blue: b, alpha: a)
    }
    
    @nonobjc public class var primary: UIColor {
        return .systemGreen
    }
    
    @nonobjc public class var primaryDark: UIColor {
        return UIColor(hex: "#5B8459")!
    }
    
    @nonobjc public class var primaryLight: UIColor {
        return UIColor(hex: "#DBF9D8")!
    }
    
    @nonobjc public class var primaryVeryDark: UIColor {
        return UIColor(hex: "#194975")!
    }
}
