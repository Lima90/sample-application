//
//  UIFont+Extension.swift
//  sample-application
//
//  Created by Alessandro on 09/10/21.
//

import UIKit

extension UIFont {
    
    // MARK: - Styles
    
    // Regular
    class var regular10: UIFont {
        return UIFont.systemFont(ofSize: 10.0, weight: .regular);
    }
    
    class var regular11: UIFont {
        return UIFont.systemFont(ofSize: 11.0, weight: .regular);
    }
    
    class var regular12: UIFont {
        return UIFont.systemFont(ofSize: 12.0, weight: .regular);
    }
    
    class var regular13: UIFont {
        return UIFont.systemFont(ofSize: 13.0, weight: .regular);
    }
    
    class var regular14: UIFont {
        return UIFont.systemFont(ofSize: 14.0, weight: .regular);
    }
    
    class var regular15: UIFont {
        return UIFont.systemFont(ofSize: 15.0, weight: .regular);
    }
    
    class var regular16: UIFont {
        return UIFont.systemFont(ofSize: 16.0, weight: .regular);
    }

    // Medium
    class var medium10: UIFont {
        return UIFont.systemFont(ofSize: 10.0, weight: .medium);
    }
    
    class var medium13: UIFont {
        return UIFont.systemFont(ofSize: 13.0, weight: .medium);
    }
    
    class var medium14: UIFont {
        return UIFont.systemFont(ofSize: 14.0, weight: .medium);
    }
    
    class var medium15: UIFont {
        return UIFont.systemFont(ofSize: 15.0, weight: .medium);
    }
    
    class var medium16: UIFont {
        return UIFont.systemFont(ofSize: 16.0, weight: .medium);
    }
    
    class var medium17: UIFont {
        return UIFont.systemFont(ofSize: 17.0, weight: .medium);
    }
    
    class var medium20: UIFont {
        return UIFont.systemFont(ofSize: 20.0, weight: .medium);
    }
    
    class var medium25: UIFont {
        return UIFont.systemFont(ofSize: 25.0, weight: .medium);
    }
    
    // Bold
    class var bold13: UIFont {
        return UIFont.systemFont(ofSize: 13.0, weight: .bold);
    }
    
    class var bold14: UIFont {
        return UIFont.systemFont(ofSize: 14.0, weight: .bold);
    }
    
    class var bold15: UIFont {
        return UIFont.systemFont(ofSize: 15.0, weight: .bold);
    }
    
    class var bold17: UIFont {
        return UIFont.systemFont(ofSize: 17.0, weight: .bold);
    }
    
    class var bold20: UIFont {
        return UIFont.systemFont(ofSize: 20.0, weight: .bold);
    }
    
    class var bold25: UIFont {
        return UIFont.systemFont(ofSize: 25.0, weight: .bold);
    }
    
    // Black
    class var black13: UIFont {
        return UIFont.systemFont(ofSize: 13.0, weight: .black);
    }
    
    class var black14: UIFont {
        return UIFont.systemFont(ofSize: 14.0, weight: .black);
    }
    
    class var black15: UIFont {
        return UIFont.systemFont(ofSize: 15.0, weight: .black);
    }
    
    class var black17: UIFont {
        return UIFont.systemFont(ofSize: 17.0, weight: .black);
    }
    
    class var black20: UIFont {
        return UIFont.systemFont(ofSize: 20.0, weight: .black);
    }
    
    class var black25: UIFont {
        return UIFont.systemFont(ofSize: 25.0, weight: .black);
    }

}
