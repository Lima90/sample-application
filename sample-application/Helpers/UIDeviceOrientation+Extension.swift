//
//  UIDeviceOrientation+Extension.swift
//  sample-application
//
//  Created by Alessandro on 14/10/21.
//

import UIKit
import SnapKit
import AVFoundation

extension UIDeviceOrientation {
    var videoOrientation: AVCaptureVideoOrientation? {
        
        switch self {
        case .portraitUpsideDown: return .portraitUpsideDown
        case .landscapeRight: return .landscapeLeft
        case .landscapeLeft: return .landscapeRight
        case .portrait: return .portrait
        default: return nil
        }
    }
}
